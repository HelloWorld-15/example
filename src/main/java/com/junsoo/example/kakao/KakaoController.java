package com.junsoo.example.kakao;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/kakao")
public class KakaoController {
	@RequestMapping("/login.do")
	public String kLogin() {
		return "kakao/index.html";
	}

	@RequestMapping("/main.do")
	public String kMain() {
		return "kakao/index_login.html";
	}

}
