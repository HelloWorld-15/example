package com.junsoo.example.nexon;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/nexon")
public class NexonController {

	@RequestMapping("/pc.do")
	public String pc() {
		return "nexon/index.html";
	}

	@RequestMapping("/mobile.do")
	public String mobile() {
		return "nexon/index_mobile.html";
	}

	@RequestMapping("/main.do")
	public String home() {
		return "nexon/index_home.html";
	}
}
