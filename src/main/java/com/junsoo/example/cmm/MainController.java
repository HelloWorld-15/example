package com.junsoo.example.cmm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cmm")
public class MainController {

	@RequestMapping("/main.do")
	public String main() {
		return "main.html";
	}
}
