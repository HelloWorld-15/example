package com.junsoo.example.junsoo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PortfolioController {

    @RequestMapping("/jsPortfolio.do")
    public String JsPortfolio() {
        return "junsoo/index.html";
    }
}
