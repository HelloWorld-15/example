$('.slick-container').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1, // 1개씩 슬라이드
    centerMode: true,  // 중앙배치
    variableWidth: true, // width 가 각각 다름을 표시
});
$('.slick-prev').attr('disabled', 'disabled');